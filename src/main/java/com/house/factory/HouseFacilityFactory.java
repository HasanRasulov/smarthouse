package com.house.factory;

import com.house.entity.*;

public interface HouseFacilityFactory {

	Light createLight();
	Window createWindow();
	Room createRoom(String name);
	HeatingPoint createHeatingPoint();
	
}
