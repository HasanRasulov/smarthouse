package com.house.factory;

import com.house.entity.HeatingPoint;
import com.house.entity.Light;
import com.house.entity.Room;
import com.house.entity.Window;

public class SimpleHouseFacilityFactory implements HouseFacilityFactory{

	public Light createLight() {
		return new Light();
	}

	public Window createWindow() {
		return new Window();
	}

	public Room createRoom(String name) {
		return new Room(name);
	}

	public HeatingPoint createHeatingPoint() {
		return new HeatingPoint();
	}
	

}
