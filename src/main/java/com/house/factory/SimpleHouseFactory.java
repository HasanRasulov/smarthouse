package com.house.factory;

import com.house.entity.House;
import com.house.entity.Room;

public class SimpleHouseFactory implements HouseFactory {

	HouseFacilityFactory factory;

	public SimpleHouseFactory(HouseFacilityFactory factory) {
		this.factory = factory;
	}

	public House build() {

		Room room = factory.createRoom("simple room");
		room.addLight(factory.createLight());
		room.addWindow(factory.createWindow());
		room.addHeatingPoint(factory.createHeatingPoint());

		House.house.addRoom(room);

		return House.house;

	}

}
