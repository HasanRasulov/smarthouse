package com.house.factory;

import com.house.entity.House;

public interface HouseFactory {

	House build();
	
}
