package com.house.remote;

import com.house.entity.Light;

public class LightCommandOn implements Command {

	private Light light;

	public LightCommandOn(Light light) {
		this.light = light;
	}

	public void execute() {
                light.on();
	}

	public void undo() {
		light.off();
	}

}
