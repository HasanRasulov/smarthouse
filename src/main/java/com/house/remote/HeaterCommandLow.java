package com.house.remote;

import org.apache.log4j.Logger;

import com.house.entity.*;

public class HeaterCommandLow implements Command{

	private HeatingPoint point;
	private Temperature prev;
	final static Logger logger = Logger.getLogger(HeaterCommandLow.class);

	
	public HeaterCommandLow(HeatingPoint point) {
		this.point = point;
	}
	
	public void execute() {
		  prev = point.getValue(); 
          point.low();		
          logger.info(prev.name() + " to " + point.getValue().name());
	}

	public void undo() {
		
		point.setValue(prev);
		logger.info(prev.name() + " to " + point.getValue().name());
	}

}
