package com.house.remote;

import com.house.entity.Window;

public class WindowCommandClose implements Command{

	private Window window;

	public WindowCommandClose(Window window) {
		this.window = window;
	}

	public void execute() {
        window.close(); 
	}

	public void undo() {
	
       window.open();
	}

	
}
