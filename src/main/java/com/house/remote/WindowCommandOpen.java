package com.house.remote;

import com.house.entity.Window;

public class WindowCommandOpen implements Command {

	private Window window;

	public WindowCommandOpen(Window window) {
		this.window = window;
	}

	public void execute() {
        window.open(); 
	}

	public void undo() {
	
       window.close();
	}

}
