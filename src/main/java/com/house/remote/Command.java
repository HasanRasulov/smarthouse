package com.house.remote;

public interface Command {

	public void execute();
	public void undo();
	
}
