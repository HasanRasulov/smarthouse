package com.house.remote;

import org.apache.log4j.Logger;

import com.house.entity.HeatingPoint;
import com.house.entity.Temperature;

public class HeaterCommandHigh implements Command{

	private HeatingPoint point;
	private Temperature prev;
	
	final static Logger logger = Logger.getLogger(HeaterCommandHigh.class);
	
	public HeaterCommandHigh(HeatingPoint point) {
		this.point = point;
	}
	
	public void execute() {
		  prev = point.getValue(); 
          point.high();		
          
          logger.info(prev.name() + " to " + point.getValue().name());
	}

	public void undo() {
		
		point.setValue(prev);
		logger.info(prev.name() + " to " + point.getValue().name());
	}

}
