package com.house.remote;

import com.house.entity.Light;

public class LightCommandOff implements Command {

	private Light light;

	public LightCommandOff(Light light) {
		this.light = light;
	}

	public void execute() {
                light.off();
	}

	public void undo() {
		light.on();
	}

}
