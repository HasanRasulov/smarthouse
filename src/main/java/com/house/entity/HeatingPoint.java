package com.house.entity;

import org.apache.log4j.Logger;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HeatingPoint {

    final static Logger logger = Logger.getLogger(HeatingPoint.class);
	
	Temperature value;

	public HeatingPoint() {
		value = Temperature.VERYLOW;
	}

	public void high() {
		if (value == Temperature.VERYLOW)
			value = Temperature.LOW;
		else if (value == Temperature.LOW)
			value = Temperature.MEDIUM;
		else
			value = Temperature.HIGH;

		logger.info(value.toString());
	}

	public void low() {
		if (value == Temperature.HIGH)
			value = Temperature.MEDIUM;
		else if (value == Temperature.MEDIUM)
			value = Temperature.LOW;
		else
			value = Temperature.VERYLOW;
		
		logger.info(value.toString());
	}
}
