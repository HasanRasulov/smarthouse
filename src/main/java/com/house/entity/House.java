package com.house.entity;

import java.util.ArrayList;
import lombok.Getter;

@Getter
public enum House {

	house;

	private ArrayList<Room> rooms;

	House() {
		rooms = new ArrayList<Room>();
	}

	public void addRoom(Room room) {
		rooms.add(room);
	}

}
