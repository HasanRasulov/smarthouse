package com.house.entity;

import org.apache.log4j.Logger;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Window {

	final static Logger logger = Logger.getLogger(Window.class);

	public void open() {
		logger.info("window is open");
	}

	public void close() {
		logger.info("window is close");
	}
}
