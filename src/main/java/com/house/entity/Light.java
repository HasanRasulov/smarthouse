package com.house.entity;

import org.apache.log4j.Logger;

import lombok.*;

@Getter
@Setter
@ToString
public class Light {
	
    final static Logger logger = Logger.getLogger(Light.class);


	public void on() {
        logger.info("light is on");
	}

	public void off() {
		logger.info("light is on");
	}
}
