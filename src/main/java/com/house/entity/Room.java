package com.house.entity;


import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Room {

	private double size;
	private String color;
	private String name;
	private ArrayList<Light> lights;
	private ArrayList<Window> windows;
	private ArrayList<HeatingPoint> heatingPoints;
	
	public Room(String name) {
		this.setName(name);
		lights = new ArrayList<Light>();
		windows = new ArrayList<Window>();
		heatingPoints = new ArrayList<HeatingPoint>();
	}
	
	
	public void addLight(Light light) {
		lights.add(light);
	}

	public void addWindow(Window window) {
		windows.add(window);
	}


	public void addHeatingPoint(HeatingPoint point) {
		heatingPoints.add(point);
	}

}
